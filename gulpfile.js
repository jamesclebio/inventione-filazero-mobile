var del = require('del'),
    gulp = require('gulp'),
    gutil = require('gulp-util'),
    plumber = require('gulp-plumber'),
    compass = require('gulp-compass');

gulp.task('default', ['styles']);

gulp.task('watch', function() {
  gulp.watch('styles/**/*', ['styles']);
});

gulp.task('styles', function () {
  del('www/assets/styles/*', function () {
    gulp.src('styles/**/*.sass')
      .pipe(plumber())
      .pipe(compass({
        style: 'compressed',
        sass: 'styles',
        css: 'www/assets/styles',
        font: 'www/assets/fonts',
        image: 'www/assets/images'
      }))
      .pipe(gulp.dest('www/assets/styles'));
  });
});
