# Filazero Mobile

## Ambiente de desenvolvimento

### Requisitos

* [Git](http://git-scm.com/)
* [Node.js](http://nodejs.org/)
* [npm](https://www.npmjs.org/)
* [gulp](http://gulpjs.com/)
* [Bower](http://bower.io/)
* [Ruby](https://www.ruby-lang.org/)
* [Sass](http://sass-lang.com/)
* [Compass](http://compass-style.org/)
* [Apache Cordova](https://cordova.apache.org/)
* [Ionic](http://ionicframework.com/)

### Instalação

Antes de prosseguir com a instalação do ambiente, certifique-se de que cada item mencionado em *requisitos* esteja funcionando normalmente.

Os passos a seguir devem ser executados via linha de comando:

1 - Baixe o repositório do projeto:

`git clone git@bitbucket.org:inventione/filazero-mobile.git`

2 - Entre no diretório *filazero-mobile*:

`cd filazero-mobile`

3 - Instale as dependências *gulp*:

`npm install`

4 - Instale as dependências *Bower*:

`bower install`

5 - Instale as dependências do *Ionic*:

`ionic state restore

That's all, dude! =)

### Estrutura

Instruções sobre estrutura.

### Fluxo de trabalho

Instruções sobre fluxo de trabalho.

### Guia de estilo

A Inventione adota as seguintes premissas para manter a consistência de seus códigos:

##### Geral

* Nomenclatura de pastas e arquivos no formato slug (my-sample.context)
* Nomenclatura de variáveis, atributos e métodos no formato camelCase
* Indentação com 2 espaços
* Versionamento semântico ([http://semver.org/](http://semver.org/))

O uso do plugin [EditorConfig](http://editorconfig.org/) no editor de código ajuda a manter alguns desses itens em ordem.

##### HTML

* Valores do atributo *id* no formato camelCase
* Valores dos atributos *class* e *data* no formato slug (my-class)

##### JavaScript

* Código em strict mode
* Padrão de design Module Pattern

##### Sass/CSS

* Sintaxe do Sass no formato [Sass Indented Syntax](http://sass-lang.com/documentation/file.INDENTED_SYNTAX.html)
* Nomenclatura de variáveis, mixins, placeholders e classes no formato slug