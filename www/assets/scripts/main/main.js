;(function($, window, document, undefined) {
  'use strict';

  window.main = window.main || {};

  main.init = function () {
    console.log('main init');

    var resources = [];

    if (arguments.length) {
      resources = arguments;
    } else {
      for (var i in this) {
        resources.push(i);
      }
    }

    this.initTrigger(this, resources);
  };

  main.initTrigger = function (object, resources) {
    if (!resources) {
      resources = [];

      for (var i in object) {
        resources.push(i);
      }
    }

    for (var j in resources) {
      if (object[resources[j]].hasOwnProperty('settings') && object[resources[j]].settings.autoinit) {
        object[resources[j]].init();
      }
    }
  };

  main.init();
}(jQuery, this, this.document));
