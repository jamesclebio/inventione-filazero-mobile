;(function ($, window, document, undefined) {
  'use strict';

  window.main = window.main || {};

  main.layout = {
    settings: {
      autoinit: true
    },

    init: function () {
      this.builder();
    },

    builder: function () {}
  };
}(jQuery, this, this.document));
