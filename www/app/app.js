;(function(){
  'use strict';


  angular.module('filazero-mobile', [
    'ionic',
    'ngCordova',
    'LocalStorageModule',
    'ngResource',
    'ionic.service.core',
    'ionic.service.push',
    'ionic-ratings',
    'tmh.dynamicLocale',
    'pascalprecht.translate',
    'ui.mask',
    'angular-ladda'
  ]);

})();
