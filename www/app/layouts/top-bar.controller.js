﻿;(function () {
  'use strict';

  angular
    .module('filazero-mobile')
    .controller('TopBarController', TopBarController);

  TopBarController.$inject = ['AuthService', '$state', '$ionicHistory', '$ionicSideMenuDelegate', '$rootScope', 'localStorageService'];

  function TopBarController(AuthService, $state, $ionicHistory, $ionicSideMenuDelegate, $rootScope, localStorageService) {
    var vm = this;

    vm.hasBackButton = true;
    vm.showSideMenu = true;
    vm.showLogo = true;
    vm.userData = localStorageService.get('userData');

    vm.logOut = logOut;
    vm.menu = menu;
    vm.myGoBack = myGoBack;
    vm.search = search;
    vm.selfService = selfService;
    vm.showDefaultBar = showDefaultBar;
    vm.isMenuOpened = isMenuOpened;

      init();

    function init(){
      if(vm.userData && !vm.userData.picture){
        vm.userData.picture = "assets/images/avatar-default.svg";
      }
      vm.hasBackButton = !$state.is('blank.login');
      $ionicSideMenuDelegate.canDragContent(false);
      vm.isIOS = ionic.Platform.isIOS();
    }

    function logOut() {
      AuthService.logOut();
      $state.go('blank.login');
    }

    function menu() {
      $ionicSideMenuDelegate.toggleLeft();
    }

    function myGoBack() {
      $ionicHistory.goBack();
    }

    function search() {
      $state.go('blank.explore');
    }

    function selfService() {
      $state.go('blank.self-service');
    }

    function showDefaultBar() {
      return $state.current.name !== 'blank.explore';
    }

     function isMenuOpened(){
      return $ionicSideMenuDelegate.isOpen();
    }

    $rootScope.$on('hasNotBackButton', function(){
      vm.hasBackButton = false;
    });

    $rootScope.$on('hasBackButton', function(){
      vm.hasBackButton = true;
    });

    $rootScope.$on('userDataModified', function(){
      vm.userData = localStorageService.get('userData');
      if(!vm.userData.picture){
        vm.userData.picture = "assets/images/avatar-default.svg";
      }
    })
  }
})();
