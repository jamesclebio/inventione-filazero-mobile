;(function () {
  angular
    .module('filazero-mobile')
    .factory('requestInterceptor', ['$q', '$rootScope', '$injector', '$location', '$filter', '$cordovaToast',
      function ($q, $rootScope, $injector, $location, $filter, $cordovaToast) {
        var codes = ["1004", "1005", "1008"];
        var hide = ["2133"];

        return {

          responseError: function (response) {

            if(response.status){
              if (response.status === 400) {
                if (response.data.error === '2061') {
                  $cordovaToast.show(response.data.error_description, 'long', 'bottom');
                }
              }
              if(response.status === 403){
                $cordovaToast.show("Email não confirmado", 'long', 'bottom');
              }
              if(response.status === 500){
                $cordovaToast.show("Erro inesperado. Tente novamente", 'long', 'bottom');
              }
            }
            return $q.reject(response);
          },

          response: function (response) {

            if (response.config.method !== 'GET' && response.status === 200) {

              if (response.data.messages && response.data.messages.length > 0) {

                if ((response.data.messages[0].type != "SUCCESS" && hide.indexOf(response.data.messages[0].code) === -1) || codes.indexOf(response.data.messages[0].code) !== -1) {
                  var i, msg;
                  for (i = 0; i <= response.data.messages.length - 1; i++) {

                    msg = response.data.messages[i];

                    $cordovaToast.show(msg.description, 'long', 'bottom');
                  }
                }
              }
            }

            return response || $q.when(response);
          }
        };
      }]);

  angular.module('filazero-mobile').config(function($httpProvider){$httpProvider.interceptors.push('requestInterceptor');});
}());
