;(function () {
  'use strict';

  angular
    .module('filazero-mobile')
    .run(PushConfig);

  PushConfig.$inject = ['AuthService', '$rootScope', '$ionicPlatform', '$state', '$cordovaPush', 'PushService'];

  function PushConfig(AuthService, $rootScope, $ionicPlatform, $state, $cordovaPush, PushService) {

    $ionicPlatform.ready(
      function () {

        $rootScope.$on('$cordovaPush:notificationReceived', function (event, notification) {
          console.log("Event: " + event);
          console.log("Notification: " + notification);
          if (notification.alert) {
            navigator.notification.alert(notification.alert);
          }

          if (notification.sound) {
            var snd = new Media(event.sound);
            snd.play();
          }

          if (notification.badge) {
            $cordovaPush.setBadgeNumber(notification.badge).then(function (result) {
              // Success!
            }, function (err) {
              // An error occurred. Show a message to the user
            });
          }
        });

        if(AuthService.isAuth()){
          PushService.registerDeviceOnPush();
        }

        window.onNotificationGCM = function (e) {
          switch (e.event) {
            case 'message':
              if (!e.foreground) {
                //Aplicativo minimizado
                if (e.payload.ticketId) {
                  $state.go('blank.ticket-details', {id: e.payload.ticketId});
                }
              }
              $rootScope.$broadcast('refreshTickets');
              break;
            default:
              // Aplicativo minimizado
              if (e.foreground === "0") {
                $state.go('blank.ticket-details', {id: e.ticketId});
              }
              $rootScope.$broadcast('refreshTickets');
          }
        };
      }
    );
  }
})();
