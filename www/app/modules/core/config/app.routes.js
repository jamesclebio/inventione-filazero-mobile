(function () {
  'use strict';

  angular
    .module('filazero-mobile')
    .config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

      $urlRouterProvider.otherwise('/add-tickets');

      $stateProvider

        .state('blank', {
          abstract: true,
          cache: false,
          url: '',
          templateUrl: 'app/layouts/blank.html',
          controller: 'TopBarController as vm'
        })

        .state('tabs', {
          abstract: true,
          cache: false,
          url: '/tabs',
          templateUrl: 'app/layouts/tabs.html',
          controller: 'TopBarController as vm'
        })
    }]);
})();
