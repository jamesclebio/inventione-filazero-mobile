;
(function () {
  'use strict';

  angular
    .module('filazero-mobile')
    .run(runConfig);

  runConfig.$inject = ['$ionicPlatform', 'tmhDynamicLocale', '$cordovaStatusbar', 'AuthService', '$rootScope', '$state', 'localStorageService', 'uiMaskConfig'];

  function runConfig($ionicPlatform, tmhDynamicLocale, $cordovaStatusbar, AuthService, $rootScope, $state, localStorageService, uiMaskConfig) {
    tmhDynamicLocale.set('pt-br');
    $ionicPlatform.ready(function () {
      if (window.StatusBar) {
        if (ionic.Platform.isAndroid()) {
          $cordovaStatusbar.overlaysWebView(true);
          $cordovaStatusbar.styleHex('#564790');
        }
      }
      var ticketsList = localStorageService.get('tickets');
      if(ticketsList && ticketsList.length > 0){
        $state.go('tickets-monitor')
      }else{
        $state.go('add-ticket')
      }

      var isAndroid = /(android)/i.test(navigator.userAgent);

      if (isAndroid) {
        var index = uiMaskConfig.eventsToHandle.indexOf('input');
        uiMaskConfig.eventsToHandle.splice(index, 1);
      }
    });

    $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams, options) {
      // if (toState.data.requireAuthentication && !AuthService.isAuth()) {
      //     event.preventDefault();
      //     $state.go('login', { 'redirectTo': toState.name, 'redirectToParams': toParams });
      // }
      // checkBackButton(toState.name);
    });

    function checkBackButton(state) {
      if (state === 'blank.login') {
        $rootScope.$broadcast('hasNotBackButton');
      } else {
        $rootScope.$broadcast('hasBackButton');
      }
    }
  }
}());
