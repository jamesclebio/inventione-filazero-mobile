;
(function() {
    'use strict';

    // var serviceBase = { //develop
    //     clientId: 'filazero-mobile',
    //     apiServiceBaseUri: 'https://fzapid.azurewebsites.net/',
    //     urlOrigin: 'http://localhost:37374/#/',
    //     facebookId: "1484694105127139",
    //     googleId: "758607280440-3n1tu80gikntm5079837svhlnume0k9e.apps.googleusercontent.com"
    // };

    var serviceBase = { //produção
      clientId: 'filazero-mobile',
      apiServiceBaseUri: 'https://api.filazero.net/',
      urlOrigin: 'https://localhost:37374/#/',
      facebookId: "1478038145792735",
      googleId: "758607280440-3n1tu80gikntm5079837svhlnume0k9e.apps.googleusercontent.com"
    };

    angular
        .module('filazero-mobile')
        .constant('ngAuthSettings', serviceBase);
})();
