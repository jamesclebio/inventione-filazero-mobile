;(function(){
  'use strict';

  angular
    .module('filazero-mobile')
    .config(appConfig);

  appConfig.$inject = ['tmhDynamicLocaleProvider', '$translateProvider', '$httpProvider', 'laddaProvider'];

  function appConfig(tmhDynamicLocaleProvider, $translateProvider, $httpProvider, laddaProvider){
    tmhDynamicLocaleProvider.localeLocationPattern('lib/angular-i18n/angular-locale_{{locale}}.js');
    $translateProvider.useStaticFilesLoader({
      prefix: 'app/i18n/',
      suffix: '.json'
    });

    $translateProvider.preferredLanguage('pt-br');

    laddaProvider.setOption({
      style: 'expand-left',
      spinnerSize: 35,
      spinnerColor: '#ffffff'
    });

    $httpProvider.interceptors.push('authInterceptorService');
  }
}());
