;(function(){
  'use strict';
  angular
    .module('filazero-mobile')
    .controller('ActivitiesController', ActivitiesController);

  ActivitiesController.$inject = ['$scope', '$state', 'ActivitiesService', '$rootScope'];

  function ActivitiesController($scope, $state, ActivitiesService, $rootScope){
    var vm = this;

    vm.activitiesLimit = 8;
    vm.activitiesOffset = 0;
    vm.hasMoreActivities = true;
    vm.isLoading = true;
    vm.totalActivities = 0;

    vm.activities = [];

    vm.getActivities = getActivities;
    vm.onSwipeRight = onSwipeRight;

    init();

    function init(){
      vm.notConfirmed = false;
      vm.notConnected = false;
      getActivities(true);
    }

    function getActivities(refreshed){
      if(refreshed){
        vm.activitiesOffset = 0;
      }else{
        vm.activitiesOffset = vm.activitiesOffset + vm.activitiesLimit;
        vm.isLoadingMore = true;
      }
      ActivitiesService.getUserActivities(vm.activitiesLimit, vm.activitiesOffset).then(
        function(response){
          vm.notConfirmed = false;
          vm.notConnected = false;
          if(refreshed){
            vm.activities = response.activities;
          }else{
            vm.activities = vm.activities.concat(response.activities);
            vm.isLoadingMore = false;
          }
          vm.totalActivities = response.totalActivities;
          vm.hasMoreActivities = vm.activities.length < vm.totalActivities;
        },
        function(error){
          if(error && error.status === 403){
            vm.notConfirmed = true;
            vm.notConnected = false;
          } else if(error === null || error.status === 0){
            vm.notConnected = true;
            vm.notConfirmed = false;
          }
        }
      ).finally(function(){
        vm.isLoading = false;
        $scope.$broadcast('scroll.refreshComplete');
      })
    }

    function onSwipeRight(){
      $state.go('tabs.tickets.index');
    }

    $rootScope.$on('refreshTickets',function(){
      getActivities(true);
    });

  }
})();
