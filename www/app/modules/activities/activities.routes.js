;(function(){
  'use strict';
  angular
    .module('filazero-mobile')
    .config(ActivitiesRouteConfig);

  ActivitiesRouteConfig.$inject = ['$stateProvider'];
  function ActivitiesRouteConfig($stateProvider) {

    $stateProvider

      .state('tabs.activities', {
        url: '/activities',
        data:{ requireAuthentication: true },
        views: {
          'activities': {
            templateUrl: 'app/modules/activities/activities.html',
            controller: 'ActivitiesController as vm'
          }
        }
      })
  }

})();

