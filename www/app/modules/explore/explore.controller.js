;(function () {
  'use strict';

  angular
    .module('filazero-mobile')
    .controller('ExploreController', ExploreController);

  ExploreController.$inject = ['ExploreService', '$state'];

  function ExploreController(ExploreService, $state) {
    var vm = this;

    vm.isIOS = ionic.Platform.isIOS();
    vm.isLoading = false;
    vm.lastSearchedString = '';
    vm.limit = 8;
    vm.locationsFound = 0;
    vm.offset = 0;
    vm.searched = false;

    vm.locations = [];

    vm.isMobile = isMobile;
    vm.myGoBack = myGoBack;
    vm.search = search;
    vm.showMore = showMore;

    function isMobile(location) {
      return location.type === "Mobile";
    }

    function myGoBack() {
      $state.go('tabs.tickets.index');
    }

    function search(offset, string) {
      cordova.plugins.Keyboard.close();
      if((vm.lastSearchedString !== vm.query || offset) && string && string.length > 0){
        vm.lastSearchedString = vm.query;
        if (string) {
          vm.lastSearchedString = string;
        }
        if (!offset) {
          vm.offset = 0;
          vm.isLoading = true;
        }else{
          vm.isLoadingMore = true;
        }
        if (vm.lastSearchedString.length > 0) {
          ExploreService.search(vm.lastSearchedString, vm.limit, vm.offset).then(
            function (response) {
              for(var i = 0; i < response.locations.length; i++){
                if(!response.locations[i].picture){
                  response.locations[i].picture = "assets/images/building.png";
                }
              }
              vm.searched = true;
              vm.locationsFound = response.locationsFound;
              if (offset) {
                vm.isLoadingMore = false;
                vm.locations = vm.locations.concat(response.locations);
              } else {
                vm.locations = response.locations;
              }
              vm.isLoading = false;
            }
          )
        }
      }
    }

    function showMore() {
      vm.offset = vm.offset + vm.limit;
      search(vm.offset, vm.lastSearchedString);
    }
  }
})();
