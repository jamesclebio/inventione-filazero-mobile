;(function () {
  'use strict';

  angular
    .module('filazero-mobile')
    .config(config);

  config.$inject = ['$stateProvider'];

  function config($stateProvider) {
    $stateProvider.state('blank.explore', {
      url: '/explore',
      data:{ requireAuthentication: true },
      templateUrl: 'app/modules/explore/explore.html',
      controller: 'ExploreController as vm'
    });
  }
})();
