﻿;(function () {
  'use strict';

  angular
    .module('filazero-mobile')
    .controller('SignupController', SignUpController);

  SignUpController.$inject = ['$state', 'AuthService', 'CordovaService'];

  function SignUpController($state, AuthService, CordovaService) {
    var vm = this;

    vm.user = {
      checkTerms: true
    };
    vm.isLoading = false;

    vm.register = register;

    function register() {
      vm.isLoading = true;
      AuthService.registerUser(vm.user).then(
        function (response) {
          if (response.messages[0].type == "SUCCESS") {
            $state.go('blank.registred');
          }
          if(response.messages[0].type == "INFO"){
            CordovaService.showNotification(response.messages[0].description, 'long', 'bottom');
          }
        }
      ).finally(function(){
        vm.isLoading = false;
      })
    }

  }
})();
