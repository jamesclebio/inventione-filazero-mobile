;(function () {
  'use strict';
  angular
    .module('filazero-mobile')
    .config(['$stateProvider', '$urlRouterProvider', function ($stateProvider) {

      $stateProvider

        .state('blank.signup', {
          url: '/signup',
          cache: false,
          data:{ requireAuthentication: false },
          templateUrl: 'app/modules/signup/signup.html',
          controller: 'SignupController as vm'
        })

        .state('blank.registred', {
          url: '/registred',
          cache: false,
          data:{ requireAuthentication: false },
          templateUrl: 'app/modules/signup/registred.html'
        })

    }]);

})();
