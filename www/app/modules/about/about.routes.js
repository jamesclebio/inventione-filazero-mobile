;(function () {
  'use strict';

  angular
    .module('filazero-mobile')
    .config(config);

  config.$inject = ['$stateProvider'];

  function config($stateProvider) {
    $stateProvider
      .state('blank.about', {
        url: '/about',
        data:{ requireAuthentication: false },
        templateUrl: 'app/modules/about/about.html',
        controller: 'AboutController as vm'
      })

      .state('blank.terms', {
        url: '/terms',
        data:{ requireAuthentication: false },
        templateUrl: 'app/modules/about/terms.html'
      })

      .state('blank.info', {
        url: '/info',
        data:{ requireAuthentication: false },
        templateUrl: 'app/modules/about/info.html'
      });
  }
})();
