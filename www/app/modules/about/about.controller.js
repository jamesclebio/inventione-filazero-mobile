;(function () {
  'use strict';

  angular
    .module('filazero-mobile')
    .controller('AboutController', AboutController);

  AboutController.$inject = ['$scope', '$cordovaAppVersion', '$ionicModal', 'ngAuthSettings'];

  function AboutController($scope, $cordovaAppVersion, $ionicModal, ngAuthSettings) {
    var vm = this;

    vm.versionModal = versionModal;

    function versionModal(){
      $ionicModal.fromTemplateUrl('app/modules/about/version-details-modal.html',{
        scope: $scope,
        animation: 'slide-in-up'
      }).then(
        function(modal){
          $cordovaAppVersion.getVersionNumber().then(
            function(version){
              vm.version = version;
              checkEnvironment();
            }
          );
          vm.appName = "Filazero";
          vm.modal = modal;
          vm.modal.show();
        }
      );
    }

    function checkEnvironment(){
      var environment = '';
      switch(ngAuthSettings.apiServiceBaseUri){
        case 'https://api.filazero.net/':
          environment = '';
          break;
        case 'https://fzapid.azurewebsites.net/':
          environment = 'd';
          break;
        case 'https://fzapih.azurewebsites.net/':
          environment = 'h';
          break;
      }
      vm.version += environment;
    }
  }
})();
