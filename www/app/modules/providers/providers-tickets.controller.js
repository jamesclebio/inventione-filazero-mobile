﻿;(function () {
  'use strict';

  angular
    .module('filazero-mobile')
    .controller('ProvidersTicketsController', ProvidersTicketsController);

  ProvidersTicketsController.$inject = ['$scope', '$stateParams', 'TicketsService'];

  function ProvidersTicketsController($scope, $stateParams, TicketsService) {
    var vm = this;

    vm.getTicketsByProviders = getTicketsByProviders();
    vm.ticketOpen = ticketOpen;

    init();

    function init() {
      getTicketsByProviders();
    }

    function getTicketsByProviders() {
      vm.isLoading = true;
      vm.hasProvider = false;
      TicketsService.getTicketByProvider($stateParams.id).then(
        function (provider) {
          if (provider != null) {
            vm.provider = provider;
            vm.hasProvider = true;
          } else {
            vm.hasProvider = false;
          }
        }).finally(function(){
        vm.isLoading = false;
        $scope.$broadcast('scroll.refreshComplete');
      });
    }

    function ticketOpen(ticket) {
      return ticket.status === "OPENED";
    }

  }
})();
