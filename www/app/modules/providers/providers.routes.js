﻿;(function(){
  'use strict';

  angular
    .module('filazero-mobile')
    .config(ProvidersRouteConfig);

  ProvidersRouteConfig.$inject = ['$stateProvider'];

  function ProvidersRouteConfig($stateProvider) {
    $stateProvider

      .state('tabs.providers', {
        url: '/providers',
        data:{ requireAuthentication: true },
        views: {
          'providers': {
            templateUrl: 'app/modules/providers/providers.html',
            controller: 'ProvidersController as vm'
          }
        }
      })

      .state('tabs.providers.index', {
        url: '/providers',
        data:{ requireAuthentication: true },
        views: {
          providers: {
            templateUrl: 'app/modules/providers/providers.html',
            controller: 'ProvidersController as vm'
          }
        }
      })

      .state('blank.providers-tickets', {
        url: '/providers/:id/tickets',
        params: {
          id: null
        },
        data:{ requireAuthentication: true },
        templateUrl: 'app/modules/providers/providers-tickets.html',
        controller: 'ProvidersTicketsController as vm'
      })
  }

})();
