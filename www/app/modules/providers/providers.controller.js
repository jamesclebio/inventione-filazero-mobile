﻿;(function () {
  'use strict';

  angular
    .module('filazero-mobile')
    .controller('ProvidersController', ProvidersController);

  ProvidersController.$inject = ['$scope', '$state', 'TicketsService', '$rootScope'];

  function ProvidersController($scope, $state, TicketsService, $rootScope) {
    var vm = this;

    vm.getProviders = getProviders;
    vm.onSwipeLeft = onSwipeLeft;

    init();

    function init() {
      vm.isLoading = true;
      vm.hasProviders = false;
      vm.notConfirmed = false;
      vm.notConnected = false;
      vm.getProviders();
    }

    function getProviders() {
      TicketsService.getProviders().then(
        function (providers) {
          vm.notConfirmed = false;
          vm.notConnected = false;
          if (providers.length > 0) {
            vm.providers = providers;
            vm.hasProviders = true;
            for(var i = 0; i < vm.providers.length; i++){
              if(!vm.providers[i].picture){
                vm.providers[i].picture = "assets/images/building.png";
              }
            }
          } else {
            vm.hasProviders = false;
          }
        },
        function (error) {
          if(error && error.status === 403){
            vm.notConfirmed = true;
            vm.notConnected = false;
          } else if(error === null || error.status === 0){
            vm.notConnected = true;
            vm.notConfirmed = false;
          }
        }).finally(function () {
        vm.isLoading = false;
        $scope.$broadcast('scroll.refreshComplete');
      });
    }

    function onSwipeLeft() {
      $state.go('tabs.tickets.index');
    }

    $rootScope.$on('refreshTickets', function () {
      getProviders();
    });
  }

})();
