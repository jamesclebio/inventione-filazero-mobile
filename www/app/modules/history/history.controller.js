;(function () {
  'use strict';

  angular
    .module('filazero-mobile')
    .controller('HistoryController', HistoryController);

  HistoryController.$inject = ['$scope', 'TicketsService'];

  function HistoryController($scope, TicketsService) {
    var vm = this;

    vm.hasMoreTickets = true;
    vm.history = [];
    vm.historyLimit = 8;
    vm.historyOffset = 0;
    vm.isLoading = true;

    vm.getUserHistory = getUserHistory;
    vm.ticketOpen = ticketOpen;

    init();

    function init() {
      vm.getUserHistory(true);
    }

    function getUserHistory(refreshed) {
      if (refreshed) {
        vm.historyOffset = 0;
      } else {
        vm.historyOffset = vm.historyOffset + vm.historyLimit;
        vm.isLoadingMore = true;
      }
      TicketsService.getUserHistory(vm.historyLimit, vm.historyOffset).then(
        function success(response) {
          if (refreshed) {
            vm.history = response.ticketsHistory;
          } else {
            vm.history = vm.history.concat(response.ticketsHistory);
            vm.isLoadingMore = false;
          }
          vm.totalHistory = response.totalTickets;
          vm.hasMoreTickets = vm.history.length < vm.totalHistory;
          vm.isLoading = false;
          $scope.$broadcast('scroll.refreshComplete');
        },
        function () {
          vm.hasMoreTickets = false;
          vm.isLoading = false;
          vm.isLoadingMore = false;
          $scope.$broadcast('scroll.refreshComplete');
        }
      );
    }

    function ticketOpen(ticket) {
      return ticket.status === "OPENED";
    }
  }

})();
