;(function () {
  'use strict';

  angular
    .module('filazero-mobile')
    .config(HistoryRouteConfig);

  HistoryRouteConfig.$inject = ['$stateProvider'];

  function HistoryRouteConfig($stateProvider) {
    $stateProvider

      .state('blank.history', {
        url: '/history',
        cache: false,
        data:{ requireAuthentication: true },
        templateUrl: 'app/modules/history/history.html',
        controller: 'HistoryController as vm'
      })
  }
})();


