;(function () {
  'use strict';

  angular
    .module('filazero-mobile')
    .config(config);

  config.$inject = ['$stateProvider'];

  function config($stateProvider) {
    $stateProvider.state('blank.self-service', {
      url: '/p/:providerSlug/:locationId',
      data:{ requireAuthentication: true },
      templateUrl: 'app/modules/self-service/self-service.html',
      controller: 'SelfServiceController as vm'
    });
  }
})();
