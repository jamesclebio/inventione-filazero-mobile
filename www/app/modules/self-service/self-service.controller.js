;(function () {
  'use strict';

  angular
    .module('filazero-mobile')
    .controller('SelfServiceController', SelfServiceController);

  SelfServiceController.$inject = ['$scope', 'SelfServiceService', '$stateParams', '$ionicModal', 'CordovaService', 'localStorageService'];

  function SelfServiceController($scope, SelfServiceService, $stateParams, $ionicModal, CordovaService, localStorageService) {
    var vm = this;

    vm.createdSuccessfully = false;
    vm.datepickerObject = {
      titleLabel: '1. Data',
      todayLabel: 'Hoje',
      closeLabel: 'Voltar',
      setLabel: 'Selecionar',
      setButtonType : 'button-assertive',
      todayButtonType : 'button-assertive',
      closeButtonType : 'button-assertive',
      templateType: 'modal',
      showTodayButton: 'false',
      modalHeaderColor: 'bar-positive',
      modalFooterColor: 'bar-positive',
      from: new Date(new Date().getUTCFullYear(), new Date().getUTCMonth(), new Date().getUTCDate()),
      callback: function (val) {
        onDateSelected(val);
      },
      availableDates: [],
      unavailableDates: [],
      dateFormat: 'dd/MMMM/yyyy',
      closeOnSelect: true
    };
    vm.resourcesSessions = {};
    vm.return = {};
    vm.selectedSession = {};
    vm.ticket = {
      special: false
    };

    vm.availableDates = [];

    vm.clearAll = clearAll;
    vm.closeModal = closeModal;
    vm.emitTicket = emitTicket;
    vm.getDatesAvailability = getDatesAvailability;
    vm.hasSession = hasSession;
    vm.resourceById = resourceById;
    vm.selectSession = selectSession;
    vm.setTime = setTime;
    vm.showSession = showSession;
    vm.sessionExpired = sessionExpired;

    init();

    function init(){
      if(localStorageService.get('userData') && localStorageService.get('userData').phone){
        vm.ticket.phone = localStorageService.get('userData').phone;
      }
      if($stateParams.providerSlug && $stateParams.locationId){
        getLocationDetails($stateParams.providerSlug, $stateParams.locationId)
      }else{
        vm.error = true;
      }
    }

    // Limpar seleção
    function clearAll(){
      vm.createdSuccessfully = false;
      var phone = vm.ticket.phone;
      vm.ticket = {
        special: false,
        phone: phone
      };
      vm.datepickerObject.inputDate = null;
      vm.choice = undefined;
      for(var i = 0; i < vm.location.services.length; i++){
        if(vm.location.services[i].show){
          vm.ticket.serviceId = vm.location.services[i].id;
        }
      }
    }

    function closeModal(){
      $scope.modal.hide();
    }

    // Fecha os serviços selecionados e limpa os dados
    function closeOpened(serviceId){
      for(var i = 0; i < vm.location.services.length; i++){
        if(vm.location.services[i].id !== serviceId){
          vm.location.services[i].show = false;
        }
      }
      var phone = vm.ticket.phone;
      vm.ticket = {
        special: false,
        phone: phone
      };
    }

    // Método para emitir ticket
    function emitTicket(){
      if(!vm.ticket.date){
        CordovaService.showNotification("Selecione a data", 'long', 'bottom');
      } else if(!vm.ticket.sessionId){
        CordovaService.showNotification("Selecione um horário", 'long', 'bottom');
      }else if(!vm.ticket.phone){
        CordovaService.showNotification("Digite seu telefone", 'long', 'bottom');
      } else{
        vm.isLoadingTicket = true;
        SelfServiceService.emitTicket(vm.location.provider.id, vm.ticket.serviceId, vm.ticket.sessionId, vm.ticket.date, vm.ticket.special, vm.ticket.phone).then(
          function(data){
            if(data.messages){
              vm.isLoadingTicket = false;
            }else{
              vm.return = data;
              vm.createdSuccessfully = true;
            }
            vm.isLoadingTicket = false;
          }
        )
      }
    }

    // Verificar disponibilidade de datas
    function getDatesAvailability(service){
      service.show = !service.show;
      clearAll();
      closeOpened(service.id);
      if(service.show){
        vm.isLoadingService = true;
        vm.ticket.serviceId = service.id;
        SelfServiceService.getSessionsAvailability($stateParams.providerSlug, service.id).then(
          function(dates){
            vm.availableDates = [];
            vm.unavailableDates = [];
            var datesAvailability = dates;
            for (var i = 0; i < datesAvailability.length; i++) {
              if(datesAvailability[i].hasSlotLeft){
                vm.availableDates.push(new Date(datesAvailability[i].date));
              }else{
                vm.unavailableDates.push(new Date(datesAvailability[i].date));
              }
            }
            vm.datepickerObject.availableDates = vm.availableDates;
            vm.datepickerObject.unavailableDates = vm.unavailableDates;
            vm.isLoadingService = false;
          }
        );
      }
    }

    function getLocationDetails(providerSlug, locationId){
      vm.isLoadingLocation = true;
      SelfServiceService.getLocationDetails(providerSlug, locationId).then(
        function(location){
          vm.location = location;
          vm.isLoadingLocation = false;
        }
      )
    }

    function hasSession(resource){
      for(var i = 0; i < vm.resourcesSessions.sessions.length; i++){
        if(resource.id === vm.resourcesSessions.sessions[i].resourceId){
          return true;
        }
      }
      return false;
    }

    // Método executado ao selecionar a data
    function onDateSelected(val){
      if(val){
        vm.isLoadingService = true;
        vm.datepickerObject.inputDate = val;
        vm.ticket.date = val;
        vm.ticket.sessionId = '';
        SelfServiceService.getSessionsResources($stateParams.providerSlug, $stateParams.locationId, vm.ticket.serviceId, val.toJSON()).then(
          function(resourcesSessions){
            vm.resourcesSessions = resourcesSessions;
            vm.isLoadingService = false;
          }
        );
      }
    }

    // Exibir o recurso que atende na sessão
    function resourceById(resourceId){
      for(var i = 0; i < vm.resourcesSessions.resources.length; i++){
        if(vm.resourcesSessions.resources[i].id === resourceId){
          return vm.resourcesSessions.resources[i].name;
        }
      }
    }

    // Exibe o modal de sessões
    function selectSession(){
      if(vm.ticket.date){
        $ionicModal.fromTemplateUrl('app/modules/self-service/session-selector-modal.html', function($ionicModal) {
          $scope.modal = $ionicModal;
          $scope.modal.show();
          vm.selectedResource = 'noPreference';
        },{
          scope: $scope,
          animation: 'slide-in-up'
        });
      }else{
        CordovaService.showNotification("Selecione uma data", 'long', 'bottom');
      }
    }

    // Filtro de sessão
    function showSession(session){
      if(vm.selectedResource === 'noPreference'){
        return true;
      }else {
        return angular.fromJson(vm.selectedResource).id === session.resourceId;
      }
    }

    // Método executado ao selecionar a sessão
    function setTime(sessionId){
      var session = {};
      for(var i = 0; i < vm.resourcesSessions.sessions.length; i++){
        if(sessionId == vm.resourcesSessions.sessions[i].id){
          session = vm.resourcesSessions.sessions[i];
          break;
        }
      }
      if(session.hasSlotsLeft && !sessionExpired(session.end)){
        vm.ticket.sessionId = session.id;
        vm.selectedSession = session;
        $scope.modal.hide();
      }else{
        vm.choice = '';
      }
    }

    function sessionExpired(session){
      return new Date(session.end) < new Date() && session.hasSlotsLeft;
    }
  }
})();
