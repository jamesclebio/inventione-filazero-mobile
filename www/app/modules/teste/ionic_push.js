﻿var ionicPushServer = require('ionic-push-server');

var credentials = {
  IonicApplicationID: "d5386e6e",
  IonicApplicationAPIsecret: "174e877572598adcddbbbb6464935c785cfb97df4a5d2766"
};

var notification = {
  "tokens": ["DEV-0144c3b8-e2a4-4c43-ab69-75903c06bf4c"],
  "notification": {
    "alert": "Hi from Ionic Push Service!",
    "ios": {
      "badge": 1,
      "sound": "chime.aiff",
      "expiry": 1423238641,
      "priority": 10,
      "contentAvailable": true,
      "payload": {
        "key1": "value",
        "key2": "value"
      }
    }
  }
};

ionicPushServer(credentials, notification);