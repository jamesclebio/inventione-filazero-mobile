;(function () {
  'use strict';

  angular
    .module('filazero-mobile')
    .config(config);

  config.$inject = ['$stateProvider'];

  function config($stateProvider) {
    $stateProvider.state('blank.myAccount', {
      url: '/my-account',
      data: {
        requireAuthentication: true
      },
      templateUrl: 'app/modules/account/account.html',
      controller: 'AccountController as vm'
    });
  }
})();
