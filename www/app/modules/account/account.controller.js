;(function () {
  'use strict';

  angular
    .module('filazero-mobile')
    .controller('AccountController', AccountController);

  AccountController.$inject = ['$filter', '$scope', '$ionicPopup', 'AccountService', 'AuthService'];

  function AccountController($filter, $scope, $ionicPopup, AccountService, AuthService) {
    var vm = this;

    vm.days = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31];
    vm.isLoading = true;
    vm.months = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
    vm.user = {};
    vm.years = ["1930", "1931", "1932", "1933", "1934", "1935", "1936", "1937", "1938", "1939", "1940", "1941", "1942", "1943", "1944", "1945", "1946", "1947", "1948", "1949", "1950",
      "1951", "1952", "1953", "1954", "1955", "1956", "1957", "1958", "1959", "1960", "1961", "1962", "1963", "1964", "1965", "1966", "1967", "1968", "1969", "1970", "1971", "1972",
      "1973", "1974", "1975", "1976", "1977", "1978", "1979", "1980", "1981", "1982", "1983", "1984", "1985", "1986", "1987", "1988", "1989", "1990", "1991", "1992", "1993", "1994",
      "1995", "1996", "1997", "1998", "1999", "2000"];

    vm.selectGender = selectGender;
    vm.selectBirthDate = selectBirthDate;
    vm.saveChanges = saveChanges;

    init();

    function init(){
      getUserInfo();
    }

    function getUserInfo(){
      vm.isLoading = true;

      AccountService.getAccount().then(
        function(user){
          vm.notConnected = false;
          vm.user = user;
          if(vm.user.birthDateDay && vm.user.birthDateMonth && vm.user.birthDateYear){
            createBirthDate(vm.user.birthDateDay, vm.user.birthDateMonth, vm.user.birthDateYear);
          }else{
            vm.user.fullBirthDate = '';
          }
          if(vm.user.gender){
            translateGender(vm.user.gender);
          }else{
            vm.user.translatedGender = '';
          }
        },
        function(error){
          if(error === null || error.status === 0){
            vm.notConnected = true;
          }
        }
      ).finally(function(){
        vm.isLoading = false;
      });
    }

    function selectGender(){
      $ionicPopup.alert({
        templateUrl: "app/modules/account/select-gender.html",
        title: 'Gênero',
        scope: $scope
      }).then(function(){
        translateGender(vm.user.gender);
      });
    }

    function translateGender(gender){
      vm.user.translatedGender = $filter('translate')('global.genders.' + gender);
      vm.user.translatedGender = $filter('capitalize')(vm.user.translatedGender);
    }

    function selectBirthDate(){
      $ionicPopup.alert({
        templateUrl: "app/modules/account/select-birth-date.html",
        title: 'Data de nascimento',
        scope: $scope
      }).then(function(){
        createBirthDate(vm.user.birthDateDay, vm.user.birthDateMonth, vm.user.birthDateYear);
      });
    }

    function createBirthDate(date, month, year){
      vm.user.fullBirthDate = new Date(year, month, date);
      vm.user.fullBirthDate = $filter('date')(vm.user.fullBirthDate, 'shortDate');
    }

    function saveChanges(){
      vm.isLoading = true;

      AccountService.saveAccount(vm.user).then(
        function(){
          vm.notConnected = false;
          getUserInfo();
          AuthService.updateUserLocalStorageData();
        },
        function(error){
          if(error === null || error.status === 0){
            vm.notConnected = true;
          }
        }
      ).finally(function(){
        vm.isLoading = false;
      });
    }
  }
})();
