(function () {
  'use strict';

  angular
    .module('filazero-mobile')
    .config(config);

  config.$inject = ['$stateProvider'];

  function config($stateProvider) {
    $stateProvider.state('add-ticket', {
      url: '/add-ticket',
      cache: false,
      data: {
        requireAuthentication: false
      },
      templateUrl: 'app/modules/add-ticket/add-ticket.html',
      controller: 'AddTicketController as vm'
    });
  }
})();
