(function () {
  'use strict';

  angular
    .module('filazero-mobile')
    .controller('AddTicketController', AddTicketController);

  AddTicketController.$inject = ['$scope', '$state','$ionicHistory', 'AuthService', '$ionicLoading', '$ionicPopup', '$q', 'TicketsService', 'CordovaService'];

  function AddTicketController($scope, $state, $ionicHistory, AuthService, $ionicLoading, $ionicPopup, $q, TicketsService, CordovaService) {
    var vm = this;

    var popUp = {};

    vm.isAuth = AuthService.isAuth();

    vm.changedFriendlyCode = changedFriendlyCode;
    vm.selectTicket = selectTicket;
    vm.login = login;
    vm.signup = signup;
    vm.back = back;

    init();

    function init() {
      vm.friendlyCode = "";
      vm.focus = true;
      CordovaService.closeKeyboard();
    }

    function changedFriendlyCode() {
      if (vm.friendlyCode && vm.friendlyCode.length === 6) {
        searchTicket();
      }
    }

    function searchTicket() {
      CordovaService.closeKeyboard();
      vm.focus = false;
      $ionicLoading.show({
        templateUrl: 'app/layouts/loading.html'
      });

      TicketsService.getPublicTicket(vm.friendlyCode).then(
        function (response) {
          if (response.length === 0) {
            vm.friendlyCode = undefined;
            CordovaService.showNotification("Atendimento não encontrado", 'long', 'bottom');
            vm.focus = true;
          } else if (response.length === 1) {
            watchTicket(response[0]).then(
              function(){
                $state.go('tickets-monitor')
              },
              function(){
                vm.friendlyCode = undefined;
              }
            );
          }else{
            vm.tickets = response;
            popUp = $ionicPopup.show({
              templateUrl: "app/modules/add-ticket/select-ticket.html",
              title: 'Selecione o local',
              scope: $scope,
              buttons: [
                { text: 'Cancelar' }
              ]
            });
            popUp.then(
              function () {
                vm.friendlyCode = undefined;
                vm.focus = true;
              }
            )
          }
        },
        function (error) {
          vm.friendlyCode = undefined;
        }
      ).finally(function () {
        $ionicLoading.hide();
      })
    }

    function watchTicket(ticket){
      var deferred = $q.defer();

      TicketsService.watchTicket(ticket).then(
        function(response){
          deferred.resolve(response)
        },
        function(error){
          if(error === '1'){
            CordovaService.showNotification("Atendimento já adicionado", 'long', 'bottom');
          }
          if(error === '0'){
            CordovaService.showNotification("Atendimento inválido", 'long', 'bottom');
          }
          deferred.reject(error);
        }
      );

      return deferred.promise;
    }

    function selectTicket(ticket){
      watchTicket(ticket).then(
        function(){
          popUp.close();
          $state.go('tickets-monitor');
        },
        function(){
          vm.friendlyCode = undefined;
          popUp.close();
        }
      );

    }

    function back(){
      if($ionicHistory.viewHistory().backView !== null){
        $ionicHistory.goBack();
      }
    }

    function login() {
      $state.go('blank.login');
    }

    function signup() {
      $state.go('blank.signup');
    }
  }
})();
