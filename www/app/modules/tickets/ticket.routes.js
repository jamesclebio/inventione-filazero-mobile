﻿;(function () {
  angular
    .module('filazero-mobile')
    .config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

      $stateProvider
        .state('tabs.tickets', {
          abstract: true,
          url: '',
          views: {
            'tickets': {
              template: '<ion-nav-view name = "bilhetes"></ion-nav-view>'
            }
          }
        })

        .state('tabs.tickets.index', {
          url: '/tickets',
          data:{ requireAuthentication: true },
          views: {
            bilhetes: {
              templateUrl: 'app/modules/tickets/tickets.html',
              controller: 'TicketsController as vm'
            }
          }
        })

        .state('blank.ticket-details', {
          url: '/tickets/:id',
          cache: false,
          params: {
            id: null
          },
          data:{ requireAuthentication: false },
          templateUrl: 'app/modules/tickets/ticket-details.html',
          controller: 'TicketDetailsController as vm'
        })

        .state('tickets-monitor', {
          url: '/tickets-monitor',
          cache: false,
          data:{ requireAuthentication: false },
          templateUrl: 'app/modules/tickets/tickets-monitor.html',
          controller: 'TicketsMonitorController as vm'
        })
    }]);

})();
