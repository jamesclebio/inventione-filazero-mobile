(function () {
  'use strict';

  angular
    .module('filazero-mobile')
    .controller('TicketsMonitorController', TicketsMonitorController);

  TicketsMonitorController.$inject = ['$scope', '$ionicSideMenuDelegate', 'localStorageService', '$ionicHistory', '$state', 'TicketsService', 'AuthService'];
  function TicketsMonitorController($scope, $ionicSideMenuDelegate, localStorageService, $ionicHistory, $state, TicketsService, AuthService) {
    var vm = this;

    vm.ticketsGroups = [
      {name: 'next', tickets: []},
      {name: 'tomorrow', tickets: []},
      {name: 'today', tickets: []},
      {name: 'yesterday', tickets: []},
      {name: 'previous', tickets: []}
    ];
    vm.isAuth = AuthService.isAuth();


    vm.getWatchedTickets = getWatchedTickets;
    vm.showStatusBadge = showStatusBadge;
    vm.ticketFinalized = ticketFinalized;
    vm.ticketLate = ticketLate;
    vm.statusBadge = statusBadge;
    vm.showTimeLeft = showTimeLeft;
    vm.isLate = isLate;
    vm.showPrevision = showPrevision;
    vm.ticketCalled = ticketCalled;
    vm.showMenu = showMenu;
    vm.logOut = logOut;

    init();

    function init() {
      getWatchedTickets();
      vm.userData = localStorageService.get('userData');
      if(vm.userData && !vm.userData.picture){
        vm.userData.picture = "assets/images/avatar-default.svg";
      }
      vm.isIOS = ionic.Platform.isIOS();
    }

    function getWatchedTickets() {
      vm.isLoading = true;
      var tickets = [];
      TicketsService.getTickets().then(
        function (groupedTickets) {
          _(groupedTickets).forEach(function (group) {
            tickets = tickets.concat(group.tickets);
            for (var i = 0; i < group.tickets.length; i++) {
              group.tickets[i].companyAbreviation = generateProviderIcon(group.tickets[i].companyName);
            }
          });
          vm.ticketsGroups = groupedTickets;

        }
      ).finally(function () {
        vm.isLoading = false;
        if (tickets.length === 0) {
          $state.go('add-ticket');
        }
        if (tickets.length === 1) {
          $ionicHistory.clearHistory();
        }
        $scope.$broadcast('scroll.refreshComplete');
      })
    }

    function generateProviderIcon(providerName) {
      for (var i = 0; i < providerName.length; i++) {
        if (providerName[i] === " ") {
          break;
        }
      }
      if (i < providerName.length) {
        return providerName[0] + providerName[i + 1];
      } else {
        return providerName[0] + providerName[1];
      }
    }

    function ticketFinalized(ticket, ticketsGroupName) {
      return ticket.status.toLowerCase() === 'canceled' || ticket.status.toLowerCase() === 'completed' || ticketsGroupName === 'yesterday' || ticketsGroupName === 'previous';
    }

    function ticketCalled(ticket, ticketGroupName) {
      return ticket.numberOfCalls !== 0 && (ticket.status.toLowerCase() === 'called' || ticket.status.toLowerCase() === 'reopened') && (ticketGroupName !== 'previous' && ticketGroupName !== 'yesterday');
    }

    function ticketLate(ticket) {
      return isLate(ticket) && ticket.numberOfCalls === 0 && ticket.status.toLowerCase() !== 'canceled';
    }

    //Badge
    function showStatusBadge(ticketsGroupName, ticket) {
      return ticketFinalized(ticket, ticketsGroupName) || ticket.numberOfCalls !== 0 || showTimeLeft(ticket) && !isLate(ticket);
    }

    function statusBadge(ticket, ticketGroupName) {
      if (ticket.status.toLowerCase() === 'canceled' || ticket.status.toLowerCase() === 'completed' || ticket.status.toLowerCase() === 'started') {
        return 'global.status.' + ticket.status.toLowerCase();
      }
      if (showTimeLeft(ticket) && ticket.numberOfCalls === 0) {
        return 'Faltam ' + calculateTimeLeft(ticket.forecastTime) + ' min';
      }
      if (ticketCalled(ticket, ticketGroupName)) {
        return 'Chamado ' + ticket.numberOfCalls + 'x';
      } else {
        return 'global.status.expired';
      }
    }

    function showTimeLeft(ticket) {
      if (ticket.forecastTime && ticket.status !== 'completed' && ticket.status !== 'canceled') {
        var now = new Date();
        now.setHours(0, 0, 0, 0);
        var forecastTime = new Date(ticket.forecastTime);
        forecastTime.setHours(0, 0, 0, 0);
        return now.getTime() === forecastTime.getTime() && calculateTimeLeft(ticket.forecastTime) <= 60 && calculateTimeLeft(ticket.forecastTime) > 0;
      }
    }

    function calculateTimeLeft(forecastTime) {
      return ((new Date(forecastTime).getTime() - new Date().getTime()) / 1000 / 60) << 0;
    }

    // Prevision
    function showPrevision(ticketsGroupName, ticket) {
      return !ticketFinalized(ticket, ticketsGroupName) && ticket.forecastTime && ticket.status.toLowerCase() !== 'started'
    }

    function isLate(ticket) {
      var now = new Date();
      var forecastTime = new Date(ticket.forecastTime);

      var today = new Date();
      today.setHours(0, 0, 0, 0);
      var ticketDay = new Date(ticket.forecastTime);
      ticketDay.setHours(0, 0, 0, 0);

      return today.getTime() === ticketDay.getTime() && now.getHours() - forecastTime.getHours() >= 1 || (now.getHours() === forecastTime.getHours() && now.getMinutes() > forecastTime.getMinutes());
    }

    function showMenu() {
      $ionicSideMenuDelegate.toggleLeft();
    }

    function logOut() {
      AuthService.logOut();
      $state.go('add-ticket');
    }

  }
})();
