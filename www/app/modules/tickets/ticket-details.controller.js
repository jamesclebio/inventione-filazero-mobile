﻿;(function () {
  'use strict';

  angular
    .module('filazero-mobile')
    .controller('TicketDetailsController', TicketsDetailsController);

  TicketsDetailsController.$inject = ['$scope', 'TicketsService', '$stateParams', 'AuthService'];

  function TicketsDetailsController($scope, TicketsService, $stateParams, AuthService) {
    var vm = this;

    vm.feedbackAvailable = false;
    vm.hasFeedback = false;
    vm.isLoading = true;
    vm.isLoadingActions = false;
    vm.ticket = {};
    vm.isAuth = AuthService.isAuth();

    vm.actionOnTicket = actionOnTicket;
    vm.canCancelTicket = canCancelTicket;
    vm.cancelTicket = cancelTicket;
    vm.canCheckInTicket = canCheckInTicket;
    vm.canConfirmTicket = canConfirmTicket;
    vm.checkTicket = checkTicket;
    vm.confirmTicket = confirmTicket;
    vm.getTicket = getTicket;

    init();

    function init(){
      vm.isLoading = true;
      vm.notConfirmed = false;
      vm.notConnected = false;
      vm.getTicket($stateParams.id);
    }

    function actionOnTicket(ticketId, action){
      TicketsService.buttonFunction(vm.ticket.provider.id, ticketId, action).then(
        function(){
          vm.notConfirmed = false;
          vm.notConnected = false;
          getTicket(ticketId);
        },
        function(error){
          if(error && error.status === 403){
            vm.notConfirmed = true;
            vm.notConnected = false;
          } else if(error === null || error.status === 0){
            vm.notConnected = true;
            vm.notConfirmed = false;
          }
        }
      ).finally(function(){
        vm.isLoadingActions = false;
      })
    }

    function canCancelTicket(ticket){
      return canDoAction(ticket, 'cancel');
    }

    function cancelTicket(ticketId){
      vm.isLoadingActions = true;
      actionOnTicket(ticketId, 'cancel');
    }

    function canCheckInTicket(ticket){
      return canDoAction(ticket, 'checkin');
    }

    function canConfirmTicket(ticket){
      return canDoAction(ticket, 'confirm');
    }

    function canDoAction(ticket, action){
      if(ticket.actions){
        for(var i = 0; i < ticket.actions.length; i++){
          if(ticket.actions[i].toLowerCase() === action){
            return true;
          }
        }
        return false;
      }
      else{
        return false;
      }
    }

    function checkTicket(ticketId){
      vm.isLoadingActions = true;
      actionOnTicket(ticketId, 'checkin');
    }

    function confirmTicket(ticketId){
      vm.isLoadingActions = true;
      actionOnTicket(ticketId, 'confirm');
    }

    function getTicket(ticketId){
      vm.notConfirmed = false;
      vm.notConnected = false;
      TicketsService.getTicket(ticketId).then(
        function(ticket){
          vm.notConfirmed = false;
          vm.notConnected = false;
          if(ticket.id){
            vm.ticket = ticket;
            if(vm.ticket.feedback){
              vm.hasFeedback = vm.ticket.feedback.lastUpdate;
              vm.canFeedBack = !vm.ticket.feedback.lastUpdate;
            }else{
              vm.hasFeedback = false;
              vm.canFeedBack = false;
            }
          }
        },
        function(error){
          if(error && error.status === 403){
            vm.notConfirmed = true;
            vm.notConnected = false;
          } else if(error === null || error.status === 0){
            vm.notConnected = true;
            vm.notConfirmed = false;
          }
        }
      ).finally(function(){
        vm.isLoading = false;
        $scope.$broadcast('scroll.refreshComplete');
      });
    }
  }

})();
