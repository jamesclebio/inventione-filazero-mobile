﻿;(function () {
  'use strict';

  angular
    .module('filazero-mobile')
    .controller('TicketsController', TicketsController);

  TicketsController.$inject = ['$scope', 'TicketsService', '$state', '$rootScope'];
  function TicketsController($scope, TicketsService, $state, $rootScope) {
    var vm = this;

    vm.tickets = false;
    vm.isLoading = true;
    vm.hasHistory = false;

    vm.getTickets = getTickets;
    vm.onSwipeLeft = onSwipeLeft;
    vm.onSwipeRight = onSwipeRight;
    vm.ticketOpen = ticketOpen;

    init();

    function init() {
      vm.notConfirmed = false;
      vm.notConnected = false;
      getTickets();
    }

    function getTickets() {
      TicketsService.getTickets().then(
        function (tickets) {
          vm.notConfirmed = false;
          vm.notConnected = false;
          if (tickets.length > 0) {
            vm.tickets = tickets;
            vm.hasTickets = true;
          } else{
            vm.hasTickets = false;
          }
        },
        function (error) {
          if(error && error.status === 403){
            vm.notConfirmed = true;
            vm.notConnected = false;
          } else if(error === null || error.status === 0){
            vm.notConnected = true;
            vm.notConfirmed = false;
          }
        }
      ).finally(function(){
        vm.isLoading = false;
        $scope.$broadcast('scroll.refreshComplete');
      })
    }

    function ticketOpen(ticket) {
      return ticket.status.toLowerCase() === "open" || "authorized";
    }

    function onSwipeLeft() {
      $state.go('tabs.activities');
    }

    function onSwipeRight() {
      $state.go('tabs.providers.index');
    }

    $rootScope.$on('refreshTickets', function () {
      getTickets();
    });
  }
})();
