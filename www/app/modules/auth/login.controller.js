;
(function() {

    'use strict';

    angular
        .module('filazero-mobile')
        .controller('LoginController', LoginController);

    LoginController.$inject = ['$scope', 'AuthService', '$state', '$ionicHistory', 'CordovaService'];

    function LoginController($scope, AuthService, $state, $ionicHistory, CordovaService) {

        var vm = this;

        vm.user = {};

        vm.facebookLogin = facebookLogin;
        vm.googleLogin = googleLogin;
        vm.login = login;

        init();

        function init() {
            if (AuthService.isAuth()) {
                $state.go('tabs.tickets.index');
            }
        }

        function afterLogin() {
            $ionicHistory.clearCache();
            $ionicHistory.clearHistory();
            $state.go('tickets-monitor', {}, { reload: true });
        }

        function facebookLogin() {
            vm.isLoading = true;
            AuthService.facebookLogin().then(
                function() {
                    afterLogin();
                },
                function(error) {
                  CordovaService.showNotification(JSON.stringify(error));
                }
            ).finally(function() {
                vm.isLoading = false;
            });
        }

        function googleLogin() {
            vm.isLoading = true;
            AuthService.googleLogin().then(
                function(response) {
                    afterLogin();
                },
                function(error) {
                  CordovaService.showNotification(JSON.stringify(error));
                    vm.isLoading = false;
                }
            );
        }

        function login() {
            vm.isLoading = true;
            AuthService.login(vm.user).then(
                function(response) {
                    afterLogin();
                },
                function(error) {
                    vm.isLoading = false;
                });
        }

        $scope.$on("$ionicView.enter", function() {
            $ionicHistory.clearHistory();
            $ionicHistory.clearCache();
        });
    }
})();
