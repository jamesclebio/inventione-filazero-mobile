;(function () {
  'use strict';

  angular
    .module('filazero-mobile')
    .config(LoginRouteConfig);

  LoginRouteConfig.$inject = ['$stateProvider'];

  function LoginRouteConfig($stateProvider) {
    $stateProvider

      .state('blank.login', {
        url: '/login',
        cache: false,
        data:{ requireAuthentication: false },
        templateUrl: 'app/modules/auth/login.html',
        controller: 'LoginController as vm'
      })
  }
})();

