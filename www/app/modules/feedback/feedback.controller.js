;(function(){
  'use strict';

  angular
    .module('filazero-mobile')
    .controller('FeedbackController', FeedbackController);

  FeedbackController.$inject = ['$scope', '$stateParams', 'FeedbackService', 'CordovaService', '$state'];

  function FeedbackController($scope, $stateParams, FeedbackService, CordovaService, $state){
    var vm = this;

    vm.rated = false;
    vm.hasUpdated = false;
    vm.error = false;
    vm.feedbackId = $stateParams.feedbackId;

    vm.getFeedback = getFeedback;
    vm.seeFeedback = seeFeedback;
    vm.sendFeedback = sendFeedback;

    init();

    function init(){
      getFeedback(false);
    }

    // Recuperar feedback
    function getFeedback(refresh) {
      if(!refresh){
        vm.isLoading = true;
      }
      FeedbackService.getFeedback($stateParams.feedbackId, $stateParams.guid).then(
        function(feedback){
          //Serviço avaliado
          createFeedback(feedback);
          vm.hasUpdated = feedback.lastUpdate;
          vm.isLoading = false;
          $scope.$broadcast('scroll.refreshComplete');
        },
        function(error){
          vm.isLoading = false;
          createFeedback(null);
          $scope.$broadcast('scroll.refreshComplete');
        }
      );
    }

    // Enviar feedback
    function sendFeedback() {
      if(vm.feedback.rate.rating <= 0){
        CordovaService.showNotification("É necessário selecionar uma nota", 'medium', 'bottom')
      }else{
        vm.isSendingFeedback = true;
        var feedbackSent = {
          id: $stateParams.feedbackId,
          guid: $stateParams.guid,
          rate: vm.feedback.rate.rating,
          comment: vm.feedback.comment,
          platform: ionic.Platform.platform()
        };
        FeedbackService.feedback(feedbackSent).then(
          function(){
            vm.rated = true;
            vm.error = false;
            vm.isSendingFeedback = false;
          },
          function(){
            vm.rated = true;
            vm.error = true;
            vm.isSendingFeedback = false;
          }
        );
      }
    }

    // Método interno para criação do objeto Feedback
    function createFeedback(feedback){
      vm.feedback = {
        ticket: feedback.ticket || null,
        comment: feedback.comment || "",
        lastUpdate: feedback.lastUpdate || undefined,
        rate: {
          minRating: -1,
          rating: feedback.rate || -1,
          callback: function (rating) {
            ratingsCallback(rating);
          }
        }
      };

      if(feedback && feedback.lastUpdate){
        vm.feedback.rate.readOnly = true;
      }
    }

    // Método padrão assim que a nota é dada
    function ratingsCallback(rating) {
      if(!vm.rated && !vm.hasUpdated){
        vm.feedback.rate.rating = rating;
      }
    }

    // Método para recuperar o feedback depois de enviar
    function seeFeedback(){
      vm.rated = false;
      vm.getFeedback(false);
    }
  }

})();
