;(function () {
  'use strict';

  angular
    .module('filazero-mobile')
    .config(FeedbackRouteConfig);

  FeedbackRouteConfig.$inject = ['$stateProvider'];

  function FeedbackRouteConfig($stateProvider) {
    $stateProvider

      .state('blank.feedback', {
        url: '/feedback/:feedbackId?:guid',
        data:{ requireAuthentication: true },
        templateUrl: 'app/modules/feedback/feedback.html',
        controller: 'FeedbackController as vm'
      })
  }
})();


