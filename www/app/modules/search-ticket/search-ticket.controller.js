;(function(){
  'use strict';
  angular
    .module('filazero-mobile')
    .controller('SearchTicketController', ['$scope', '$state',
      function SearchTicketController($scope, $state) {

        $scope.ticket = {
          friendlyCode: ''
        };

        $scope.search = function() {
          if ($scope.ticket.friendlyCode) {
            $state.go('page.ticket-details', {
              'friendlyCode': $scope.ticket.friendlyCode,
              'actions': null
            });
          }
        }

      }
    ]);

}());
