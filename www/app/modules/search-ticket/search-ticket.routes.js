﻿;(function () {
  'use strict';

  angular
    .module('filazero-mobile')
    .config(SearchTicketRouteConfig);

  SearchTicketRouteConfig.$inject = ['$stateProvider'];

  function SearchTicketRouteConfig($stateProvider) {
    $stateProvider

      .state('page.search', {
        url: '/search',
        views: {
          page: {
            templateUrl: 'app/modules/search-ticket/search-ticket.html',
            controller: 'SearchTicketController'
          }
        }
      })
  }
}());


