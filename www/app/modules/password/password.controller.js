;(function () {
  'use strict';

  angular
    .module('filazero-mobile')
    .controller('PasswordController', PasswordController);

  PasswordController.$inject = ['AccountService', '$state', 'CordovaService'];

  function PasswordController(AccountService, $state, CordovaService) {
    var vm = this;

    vm.passwordObject = {};

    vm.editPassword = editPassword;

    function editPassword(){
      vm.isLoading = true;
      AccountService.changePassword(vm.passwordObject).then(
        function(response){
          if(response.succeeded){
            CordovaService.showNotification("Senha alterada com sucesso", 'long', 'bottom');
            $state.go('blank.myAccount');
          }
        },
        function(error){
          if(error === null || error.status === 0){
            CordovaService.showNotification("Você parece estar desconectado. Tente novamente", 'long', 'bottom');
          }
        }
      ).finally(function(){
        vm.isLoading = false;
      })
    }
  }
})();
