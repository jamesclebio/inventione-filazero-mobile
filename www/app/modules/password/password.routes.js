;(function () {
  'use strict';

  angular
    .module('filazero-mobile')
    .config(config);

  config.$inject = ['$stateProvider'];

  function config($stateProvider) {
    $stateProvider.state('blank.password', {
      url: '/password',
      data: {
        requireAuthentication: true
      },
      templateUrl: 'app/modules/password/password.html',
      controller: 'PasswordController as vm'
    });
  }
})();
