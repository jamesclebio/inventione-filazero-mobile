;(function () {
  angular
    .module('filazero-mobile')
    .factory('ExploreService', ExploreService);

  ExploreService.$inject = ['$resource', '$q', 'ngAuthSettings'];

  function ExploreService($resource, $q, ngAuthSettings) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var resourceExplore = $resource(serviceBase + 'api/search', {}, {'update': {method: 'PUT'}});

    return {
      search: search
    };

    function search(string, limit, offset) {
      var deferred = $q.defer();

      resourceExplore.get({query: string, limit: limit, offset: offset},
        function (data) {
          deferred.resolve(data);
        },
        function (data) {
          deferred.reject(data);
        }
      );

      return deferred.promise;
    }
  }
}());
