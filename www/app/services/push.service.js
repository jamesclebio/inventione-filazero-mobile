;(function () {
  'use strict';

  angular
    .module('filazero-mobile')
    .factory('PushService', PushService);

  PushService.$inject = [
    '$resource',
    '$q',
    'ngAuthSettings',
    'localStorageService'
  ];

  function PushService($resource, $q, ngAuthSettings, localStorageService) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var resourceDevice = $resource(serviceBase + 'api/me/devices', {}, {'update': {method: 'PUT'}});

    return {
      registerDevice: registerDevice,
      registerDeviceOnPush: registerDeviceOnPush
    };

    function registerDeviceOnPush() {
      var deferred = $q.defer();
      var pushNotification = window.plugins.pushNotification;

      pushNotification.register(
        function (response) {
          window.onNotificationGCM = function (e){
            if(e.event === 'registered'){
              var device = {
                registrationId: e.regid
              };
              if (ionic.Platform.isAndroid()) {
                device.platform = "android";
              } else {
                device.platform = 'ios';
              }
              registerDevice(device).then(
                function(response){
                  deferred.resolve(response);
                },
                function(error){
                  deferred.reject(error);
                }
              );
            }
          };
        },
        function (error) {
          deferred.reject(error);
        },
        {"senderID": "758607280440", "ecb": "onNotificationGCM"});

      return deferred.promise;
    }

    function registerDevice(registerDevice) {

      var deferred = $q.defer();

      resourceDevice.save({}, registerDevice,
        function (response) {
          localStorageService.set('deviceData', registerDevice);
          deferred.resolve(response);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;

    }

  }
})();
