;(function () {
  'use strict';

  angular
    .module('filazero-mobile')
    .factory('SelfServiceService', SelfServiceService);

  SelfServiceService.$inject = ['$resource', '$q', 'ngAuthSettings', 'AuthService'];

  function SelfServiceService($resource, $q, ngAuthSettings, AuthService) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var resourceSelfService = $resource(serviceBase + 'api/providers/:providerId/self-service/sessions/:sessionId/tickets', {providerId: '@providerId', sessionId: '@sessionId'}, {'update': {method: 'PUT'}});
    var resourceLocation = $resource(serviceBase + 'api/public/providers/slug/:providerSlug', {providerId: '@providerId'}, {'update': {method: 'PUT'}});
    var resourceSessions = $resource(serviceBase + 'api/self-service/providers/:providerSlug/services/:serviceId/available-session-days', {providerSlug: '@providerSlug', serviceId: '@serviceId'}, {'update': {method: 'PUT'}});
    var resourceResources = $resource(serviceBase + 'api/self-service/providers/:providerSlug/locations/:locationId/services/:serviceId/sessions-resources-by-service', {providerSlug: '@providerSlug', serviceId: '@serviceId'}, {'update': {method: 'PUT'}});

    return {
      emitTicket: emitTicket,
      getLocationDetails: getLocationDetails,
      getSessionsAvailability: getSessionsAvailability,
      getSessionsResources: getSessionsResources
    };

    function emitTicket(providerId, serviceId, sessionId, date, special, phone) {
      var deferred = $q.defer();

      resourceSelfService.save({providerId: providerId, sessionId: sessionId}, {serviceId: serviceId, date: date, special: special, phone: phone},
        function (data) {
          AuthService.updateUserLocalStorageData();
          deferred.resolve(data);
        },
        function (error) {
          deferred.reject(error);
        }
      );

      return deferred.promise;
    }

    function getLocationDetails(providerSlug, locationId){
      var deferred = $q.defer();

      resourceLocation.get({providerSlug: providerSlug, locationId: locationId}, {},
        function (location) {
          deferred.resolve(location);
        },
        function (error) {
          deferred.reject(error);
        }
      );

      return deferred.promise;
    }

    function getSessionsAvailability(providerSlug, serviceId){
      var deferred = $q.defer();

      resourceSessions.query({providerSlug: providerSlug, serviceId: serviceId}, {},
        function (dates) {
          deferred.resolve(dates);
        },
        function (error) {
          deferred.reject(error);
        }
      );

      return deferred.promise;
    }

    function getSessionsResources(providerSlug, locationId, serviceId, date){
      var deferred = $q.defer();

      resourceResources.get({providerSlug: providerSlug, locationId: locationId, serviceId: serviceId, date: date}, {},
        function (session) {
          deferred.resolve(session);
        },
        function (error) {
          deferred.reject(error);
        }
      );

      return deferred.promise;
    }

  }
}());
