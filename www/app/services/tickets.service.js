﻿;(function () {

  'use strict';

  angular
    .module('filazero-mobile')
    .factory('TicketsService', TicketsService);

  TicketsService.$inject = ['$resource', '$q', 'ngAuthSettings', '$rootScope', 'localStorageService', 'AuthService'];

  function TicketsService($resource, $q, ngAuthSettings, $rootScope, localStorageService, AuthService) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var resourceHistory = $resource(serviceBase + 'api/me/tickets/history', {}, {'update': {method: 'PUT'}});
    var resourceTicket = $resource(serviceBase + 'api/tickets/:ticketId', {ticketId: '@ticketId'}, {'update': {method: 'PUT'}});
    var resourceTicketAction = $resource(serviceBase + 'api/providers/:providerId/tickets/:ticketId/:actionName', {
      providerId: '@providerId',
      ticketId: '@ticketId',
      actionName: '@actionName'
    }, {'update': {method: 'PUT'}});
    var resourceTicketsList = $resource(serviceBase + 'api/me/tickets/watched');
    var resourcePublicTicketsList = $resource(serviceBase + 'api/public/tickets');
    var resourcePublicTicket = $resource(serviceBase + 'api/public/tickets');
    var resourceWatchTicket = $resource(serviceBase + 'api/providers/:providerId/tickets/:ticketId/watchers', {
      providerId: '@providerId',
      ticketId: '@ticketId'
    });

    return {
      getWatchedTickets: getWatchedTickets,
      getTicket: getTicket,
      getTickets: getTickets,
      getTodayTickets: getTodayTickets,
      buttonFunction: buttonFunction,
      getUserHistory: getUserHistory,
      getPublicTicket: getPublicTicket,
      watchTicket: watchTicket,
      watchAllTickets: watchAllTickets
    };

    function getTickets() {
      var sortedTickets = [
        {name: 'next', tickets: []},
        {name: 'tomorrow', tickets: []},
        {name: 'today', tickets: []},
        {name: 'yesterday', tickets: []},
        {name: 'previous', tickets: []}
      ];
      var deferred = $q.defer();

      var today = new Date();
      today.setHours(0, 0, 0, 0);
      var yesterday = (new Date(today));
      yesterday.setDate(today.getDate() - 1);
      var tomorrow = (new Date(today));
      tomorrow.setDate(today.getDate() + 1);

      var tickets = getWatchedTickets().then(
        function (tickets) {
          _(tickets).forEach(function (ticket) {
            ticket.referenceTime = new Date(ticket.forecastTime || ticket.sessionStartTime);
            var referenceDate = new Date(ticket.referenceTime);
            referenceDate.setHours(0, 0, 0, 0);
            if (referenceDate.getTime() == today.getTime()) {
              sortedTickets[2].tickets.push(ticket);
            } else if (referenceDate.getTime() < today.getTime()) {
              if (referenceDate.getTime() < yesterday.getTime()) {
                sortedTickets[4].tickets.push(ticket);
              } else {
                sortedTickets[3].tickets.push(ticket);
              }
            } else {
              if (referenceDate.getTime() > tomorrow.getTime()) {
                sortedTickets[0].tickets.push(ticket);
              } else {
                sortedTickets[1].tickets.push(ticket);
              }
            }
          });

          _(sortedTickets).forEach(function (group) {
            group.tickets = _.orderBy(group.tickets, ['referenceTime'], ['desc']);
          });

          deferred.resolve(sortedTickets);
        },
        function (error) {
          deferred.reject(error);
        }
      );
      return deferred.promise;
    }

    function getTodayTickets() {
      var deferred = $q.defer();
      var today = new Date();
      today.setHours(0, 0, 0, 0);

      var tickets = getWatchedTickets().then(
        function (tickets) {
          var sortedTickets = _.filter(tickets, function (ticket) {
            ticket.referenceTime = new Date(ticket.forecastTime || ticket.sessionStartTime);
            ticket.referenceTime.setHours(0, 0, 0, 0);
            return ticket.referenceTime === today;
          });

          deferred.resolve([{
            name: 'today',
            tickets: sortedTickets
          }]);
        },
        function (error) {
          deferred.reject(error);
        }
      );
      return deferred.promise;
    }

    function getWatchedTickets() {
      if (AuthService.isAuth()) {
        return getUserTickets();
      } else {
        return getLocalTickets();
      }
    }

    function getUserTickets() {
      var deferred = $q.defer();

      resourceTicketsList.query({},
        function (tickets) {
          localStorageService.set('tickets', tickets);
          deferred.resolve(tickets);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function getLocalTickets() {
      var deferred = $q.defer();

      var ticketsList = [];
      _(localStorageService.get('tickets')).forEach(
        function (ticket) {
          ticketsList.push(ticket.id);
        });

      resourcePublicTicketsList.query({tickets: ticketsList},
        function (tickets) {
          localStorageService.set('tickets', tickets);
          deferred.resolve(localStorageService.get('tickets'));
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function getTicket(ticketId) {

      var deferred = $q.defer();

      resourceTicket.get({ticketId: ticketId},
        function (ticket) {
          deferred.resolve(ticket)
        }, function (error) {
          deferred.reject(error);
        }
      );

      return deferred.promise;
    }

    function buttonFunction(providerId, ticketId, actionName) {

      var deferred = $q.defer();

      resourceTicketAction.save({providerId: providerId, ticketId: ticketId, actionName: actionName},
        function (response) {
          $rootScope.$broadcast('refreshTickets');
          deferred.resolve(response);
        },
        function (error) {
          deferred.reject(error);
        }
      );

      return deferred.promise;

    }

    function getUserHistory(limit, offset) {

      var deferred = $q.defer();

      resourceHistory.get({limit: limit, offset: offset},
        function (history) {
          deferred.resolve(history)
        }, function (error) {
          deferred.reject(error);
        }
      );

      return deferred.promise;
    }

    function getPublicTicket(friendlyCode) {
      var deferred = $q.defer();

      resourcePublicTicket.query({friendlyCode: friendlyCode},
        function (response) {
          deferred.resolve(response);
        },
        function (error) {
          deferred.reject(error);
        }
      );

      return deferred.promise;
    }

    function watchTicket(ticket) {
      if (AuthService.isAuth()) {
        return watchUserTicket(ticket);
      } else {
        return watchTicketLocally(ticket);
      }
    }

    function watchUserTicket(ticket) {
      var deferred = $q.defer();

      resourceWatchTicket.save({providerId: ticket.companyId, ticketId: ticket.id},
        function (response) {
          deferred.resolve(response);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function watchTicketLocally(ticket) {
      var deferred = $q.defer();

      var ticketsList = [];
      if (ticket.id) {
        if (localStorageService.get('tickets') !== null) {
          if (!_.find(localStorageService.get('tickets'), function (ticketFromList) {
              return ticketFromList.id === ticket.id;
            })) {
            ticketsList = localStorageService.get('tickets');
            ticketsList.push(ticket);
            localStorageService.set('tickets', ticketsList);
            deferred.resolve(ticket)
          } else {
            // Ticket já adicionado
            deferred.reject('1');
          }
        } else {
          ticketsList = [ticket];
          localStorageService.set('tickets', ticketsList);
          deferred.resolve(ticket);
        }
      } else {
        // Ticket inválido
        deferred.reject('0');
      }

      return deferred.promise;
    }

    function watchAllTickets() {
      var deferred = $q.defer();

      var tickets = localStorageService.get('tickets');
      var promises = [];

      if (tickets.length > 0) {
        _.forEach(tickets, function (ticket) {
          promises.push(watchTicket(ticket));
        })
      }

      $q.all(promises).then(
        function (response) {
          deferred.resolve(response);
        },
        function (error) {
          deferred.error(error);
        }
      );

      return deferred.promise;
    }

  }
})();
