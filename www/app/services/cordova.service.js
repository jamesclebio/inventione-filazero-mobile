;(function () {
  'use strict';

  angular
    .module('filazero-mobile')
    .factory('CordovaService', CordovaService);

  CordovaService.$inject = ['$rootScope', '$timeout', '$ionicPopup', '$cordovaToast'];

  function CordovaService($rootScope, $timeout, $ionicPopup, $cordovaToast) {

    return {
      closeKeyboard: closeKeyboard,
      showNotification: show
    };

    function closeKeyboard(){
      if(!!window.cordova){
        cordova.plugins.Keyboard.close();
      }
    }

    function show(message, duration, position) {
      message = message || "There was a problem...";
      duration = duration || 'short';
      position = position || 'top';

      if (!!window.cordova) {
        $cordovaToast.show(message, duration, position);
      }
      else {
        if (duration == 'short') {
          duration = 2000;
        }
        else {
          duration = 5000;
        }

        var myPopup = $ionicPopup.show({
          template: "<div class='toast'>" + message + "</div>",
          scope: $rootScope,
          buttons: []
        });

        $timeout(function () {
          myPopup.close();
        }, duration);
      }
    }
  }
})();
