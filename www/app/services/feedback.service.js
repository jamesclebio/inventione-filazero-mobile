;(function () {
  'use strict';

  angular
    .module('filazero-mobile')
    .factory('FeedbackService', FeedbackService);

  FeedbackService.$inject = ['$resource', '$q', 'ngAuthSettings'];

  function FeedbackService($resource, $q, ngAuthSettings) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var resourceFeedback = $resource(serviceBase + 'api/feedback/:feedbackId', {feedbackId: '@feedbackId'}, {'update': {method: 'PUT'}});

    return {
      feedback: feedback,
      getFeedback: getFeedback
    };

    function feedback(feedback) {

      var deferred = $q.defer();

      resourceFeedback.update({feedbackId: feedback.id, guid: feedback.guid}, feedback,
        function (feedback) {
          deferred.resolve(feedback);
        },
        function (error) {
          deferred.reject(error);
        }
      );

      return deferred.promise;
    }

    function getFeedback(feedbackId, feedbackGuid) {
      var deferred = $q.defer();

      resourceFeedback.get({feedbackId: feedbackId, guid: feedbackGuid},
        function (feedback) {
          deferred.resolve(feedback);
        },
        function (error) {
          deferred.reject(error);
        }
      );

      return deferred.promise;
    }
  }

})();
