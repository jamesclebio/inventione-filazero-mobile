;(function () {
  'use strict';

  angular
    .module('filazero-mobile')
    .factory('AccountService', AccountService);

  AccountService.$inject = [
    '$resource',
    '$q',
    'ngAuthSettings'];

  function AccountService($resource, $q, ngAuthSettings) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var resourceAccount = $resource(serviceBase + 'api/me', {}, { 'update': { method: 'PUT' } });
    var resourceAccountDetails = $resource(serviceBase + 'api/me/profile', {}, { 'update': { method: 'PUT' } });
    var resourceAccountPassword = $resource(serviceBase + 'api/me/password', {}, { 'update': { method: 'PUT' } });

    return {
      getAccount: getAccount,
      getAccountDetails: getAccountDetails,
      saveAccount: saveAccount,
      changePassword: changePassword
    };

    function getAccount() {

      var deferred = $q.defer();

      resourceAccount.get({},
        function (data) {
          deferred.resolve(data);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function getAccountDetails(){
      var deferred = $q.defer();

      resourceAccountDetails.get({},
      function(userData){
        deferred.resolve(userData);
      },
      function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }

    function saveAccount(account) {

      var deferred = $q.defer();

      resourceAccount.update(account,
        function (data) {
          deferred.resolve(data);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function changePassword(password) {
      var deferred = $q.defer();

      resourceAccountPassword.update(password,
        function (data) {
          deferred.resolve(data);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }
  }
})();
