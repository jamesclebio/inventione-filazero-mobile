﻿;(function () {
  'use strict';
  angular
    .module('filazero-mobile')
    .factory('authInterceptorService', ['$q', '$injector', '$location', 'localStorageService', function ($q, $injector, $location, localStorageService) {

      var authInterceptorServiceFactory = {};

      var _request = function (config) {

        config.headers = config.headers || {};

        var authData = localStorageService.get('authorizationData');
        if (authData) {
          config.headers.Authorization = 'Bearer ' + authData.token;
        }

        return config;
      };

      var _responseError = function (rejection) {
        if (rejection.status === 401 && $location.path() !== '/login') {
          var AuthService = $injector.get('AuthService');
          var redirectTo = encodeURIComponent($location.url());

          var authData = localStorageService.get('authorizationData');
          if(authData === null){
            localStorageService.remove('authorizationData');
          }

          $location.url('/login').search({'redirectTo': redirectTo});

        }
        return $q.reject(rejection);
      };

      authInterceptorServiceFactory.request = _request;
      authInterceptorServiceFactory.responseError = _responseError;

      return authInterceptorServiceFactory;
    }]);

}());
