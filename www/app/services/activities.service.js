﻿;(function () {
  'use strict';

  angular
    .module('filazero-mobile')
    .factory('ActivitiesService', ActivitiesService);

  ActivitiesService.$inject = ['$resource', '$q', 'ngAuthSettings'];

  function ActivitiesService($resource, $q, ngAuthSettings) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var resourceActivities = $resource(serviceBase + 'api/me/activities', {}, { 'update': { method: 'PUT' } });

    return {
      getUserActivities: getUserActivities
    };

    function getUserActivities(limit, offset) {

      var deferred = $q.defer();

      resourceActivities.get({limit: limit, offset: offset},
        function (activities) {
          deferred.resolve(activities);
        },
        function (error) {
          deferred.reject(error);
        }
      );

      return deferred.promise;
    }
  }

})();
