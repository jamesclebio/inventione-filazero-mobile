﻿;(function () {
  'use strict';

  angular
    .module('filazero-mobile')
    .factory('AuthService', AuthService);

  AuthService.$inject = ['$rootScope', '$http', '$injector', '$q', 'localStorageService', 'ngAuthSettings', '$cordovaOauth', '$resource', 'PushService', 'AccountService'];

  function AuthService($rootScope, $http, $injector, $q, localStorageService, ngAuthSettings, $cordovaOauth, $resource, PushService, AccountService) {

    var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var authentication = {
      isAuth: false,
      userName: ""
    };

    var externalAuthData = {
      provider: "",
      userName: "",
      externalAccessToken: ""
    };

    var resourceExternalLogin = $resource(serviceBase + 'api/account/registerOrUpdateExternalLogin', {}, {'update': {method: 'PUT'}});
    var resourceRegister = $resource(serviceBase + 'api/account/register', {}, {'update': {method: 'PUT'}});

    return {
      facebookLogin: facebookLogin,
      googleLogin: googleLogin,
      isAuth: isAuth,
      login: login,
      logOut: logOut,
      registerUser: registerUser,
      updateUserLocalStorageData: updateUserLocalStorageData
    };

    function isAuth() {
      var authData = localStorageService.get('authorizationData');
      if (authData && authData.token && authData.userName) {
        authentication.isAuth = true;
        authentication.userName = authData.userName;
      }
      return authentication.isAuth;
    }

    function login(loginData) {
      var data = {
        grant_type: "password",
        client_id: ngAuthSettings.clientId,
        userName: loginData.userName,
        password: loginData.password
      };

      return token(data);
    }

    function token(data) {
      var deferred = $q.defer();

      $http.post(
          serviceBase + 'token',
        $.param(data),
        {headers: {'Content-Type': 'application/x-www-form-urlencoded'}})
        .success(
          function (response) {
            afterLogin(response.access_token, data.userName).then(
              function(response){
                deferred.resolve(response);
              },
              function(error){
                deferred.reject(error);
              }
            );
          })
        .error(
          function (error) {
            logOut();
            deferred.reject(error);
          }
        );

      return deferred.promise;
    }

    function facebookLogin() {
      var deferred = $q.defer();

      facebookConnectPlugin.login(['email', 'public_profile'],
        function (response) {
          externalAuthData.externalAccessToken = response.authResponse.accessToken;
          externalAuthData.provider = "Facebook";

          externalLogin(externalAuthData).then(
            function (response) {
              deferred.resolve(response);
            },
            function (error) {
              deferred.reject(error);
            }
          );
        }, function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function googleLogin() {
      var deferred = $q.defer();

      var clientId = ngAuthSettings.googleId;
      $cordovaOauth.google(clientId, ["email", "https://www.googleapis.com/auth/plus.me", "https://www.googleapis.com/auth/userinfo.profile"]).then(
        function (response) {
          externalAuthData.externalAccessToken = response.access_token;
          externalAuthData.provider = "Google";

          externalLogin(externalAuthData).then(
            function (response) {
              deferred.resolve(response);
            },
            function (error) {
              deferred.reject(error);
            }
          );
        }, function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function externalLogin(registerExternalData) {
      var deferred = $q.defer();

      resourceExternalLogin.save({}, registerExternalData,
        function (response) {
          afterLogin(response.access_token, response.userName).then(
            function(response){
              deferred.resolve(response);
            },
            function(error){
              deferred.reject(error);
            }
          );
        },
        function (error) {
          logOut();
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function afterLogin(accessToken, userName) {
      var deferred = $q.defer();

      setUserLocalStorage(accessToken, userName).then(
        function(){
          isAuth();
          //PushService.registerDeviceOnPush();
          $rootScope.$broadcast('userLogin');
          var TicketsService = $injector.get('TicketsService');
          TicketsService.watchAllTickets().then(
            function(response){
              deferred.resolve(response);
            },
            function(error){
              deferred.reject(error);
            }
          );
        }
      );

      return deferred.promise;
    }

    function setUserLocalStorage(accessToken, userName) {
      var deferred = $q.defer();

      localStorageService.set('authorizationData', {
        token: accessToken,
        userName: userName
      });
      updateUserLocalStorageData().then(
        function (response) {
          deferred.resolve(response);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise;
    }

    function logOut() {
      authentication.isAuth = false;
      authentication.userName = "";
      localStorageService.remove('authorizationData');
      localStorageService.remove('tickets');
      localStorageService.remove('userData');
      if (localStorageService.get('deviceData')) {
        $http.delete(serviceBase + 'api/me/devices/' + localStorageService.get('deviceData').registrationId);
      }
    }

    function registerUser(registration) {
      var deferred = $q.defer();

      resourceRegister.save({}, registration,
        function (success) {
          deferred.resolve(success);
        },
        function (error) {
          deferred.reject(error);
        });

      return deferred.promise
    }

    function updateUserLocalStorageData() {
      var deferred = $q.defer();

      AccountService.getAccountDetails().then(
        function (userData) {
          localStorageService.set('userData', userData.user);
          $rootScope.$broadcast('userDataModified');
          deferred.resolve(userData);
        },
        function(error){
          deferred.reject(error);
        }
      );

      return deferred.promise
    }

  }
})();
